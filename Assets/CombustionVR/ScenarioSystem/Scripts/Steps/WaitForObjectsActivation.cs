﻿using System.Collections.Generic;
using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using CombustionVR.Scripts;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create WaitForObjectsActivation",
        fileName = "Wait For Objects Activation Step",
        order = 0)]
    public class WaitForObjectsActivation : ScenarioStep
    {
        [SerializeField] private bool enableTargetAtStart;
        [SerializeField] private string[] objects;
        [SerializeField] private AudioClip audioToRepeatOnWait;
        [SerializeField] private string audioSourceName;
        [SerializeField] private float audioRepeatTime;

        private bool _isLaunched;
        private IStepLauncher _launcher;
        private float _previousAudioPlayTime;
        private AudioSource _audioSource;
        private readonly List<IActivatable> _activatableObjects = new List<IActivatable>();

        public override bool IsLaunched()
        {
            return _isLaunched;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            _launcher = launcher;

            _activatableObjects.Clear();

            _audioSource = _launcher.GetResources().GetAudioSource(audioSourceName);
            _audioSource.clip = audioToRepeatOnWait;
            _audioSource.loop = false;

            foreach (var gameObject in _launcher.GetResources().GetGameObjects(objects))
            {
                var handActivatedObject = gameObject.GetComponent<IActivatable>();
                _activatableObjects.Add(handActivatedObject);
                handActivatedObject.OnActivate.AddListener(OnActivated);
                if (enableTargetAtStart)
                {
                    ((MonoBehaviour) handActivatedObject).enabled = true;
                }
            }

            _previousAudioPlayTime = Time.time;
            launcher.UpdatedStep += Update;

            _isLaunched = true;
        }

        private void OnActivated(IActivatable sender)
        {
            Debug.Log(sender);

            _previousAudioPlayTime = Time.time;

            _activatableObjects.Remove(sender);
            sender.OnActivate.RemoveListener(OnActivated);

            if (_activatableObjects.Count == 0)
            {
                _launcher.StepFinished(this);
            }
        }

        private void Update()
        {
            if (_previousAudioPlayTime + audioRepeatTime < Time.time)
            {
                _audioSource.Play();
                _previousAudioPlayTime = Time.time;
            }
        }

        public override void Stop(IStepLauncher launcher)
        {
            _isLaunched = false;

#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}