using System;
using System.Collections.Generic;
using System.Linq;
using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    /// <summary>
    /// Plays once selected audio clip on specified source and passes to next step.
    /// </summary>
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create PlayAudioAndSetMaterialsByTime",
        fileName = "Play Audio And Set Materials By Time Step",
        order = 0)]
    public class PlayAudioAndSetMaterialsByTime : PlayAudioClip
    {
        [SerializeField] private MaterialChangeByTimeInfo[] timeInfoKeys;
        [SerializeField] private bool resetOnStop = true;
        private Queue<MaterialChangeByTimeInfo> _keysQueue;
        private readonly Dictionary<string, Material[]> _originalMaterials = new Dictionary<string, Material[]>();

        public override void Launch(IStepLauncher launcher)
        {
            base.Launch(launcher);

            if (!skipAudio)
            {
                _keysQueue = new Queue<MaterialChangeByTimeInfo>(timeInfoKeys.OrderBy(info => info.time));

                if (resetOnStop)
                {
                    BackupMaterials();
                }
            }
        }

        protected override void Update()
        {
            base.Update();

            if (_isLaunched)
            {
                CheckForTimedKeyInQueue();
            }
        }


        private void CheckForTimedKeyInQueue()
        {
            while (_keysQueue.Count > 0 && _keysQueue.Peek().time < AudioSource.time)
            {
                var timeInfoKey = _keysQueue.Dequeue();
                var gameObject = Launcher.GetResources().GetGameObject(timeInfoKey.targetObjectName);
                var meshRenderer = gameObject.GetComponent<MeshRenderer>();
                if (meshRenderer == null)
                {
                    var componentsInChildren = gameObject.GetComponentsInChildren<MeshRenderer>();
                    foreach (var renderer in componentsInChildren)
                    {
                        renderer.materials = timeInfoKey.materials;
                    }
                }
                else
                {
                    meshRenderer.materials = timeInfoKey.materials;
                }
            }
        }

        public override void Stop(IStepLauncher launcher)
        {
            if (resetOnStop)
            {
                RestoreMaterials();
            }

            base.Stop(launcher);
        }

        private void BackupMaterials()
        {
            foreach (var timeInfoKey in timeInfoKeys)
            {
                var targetObjectName = timeInfoKey.targetObjectName;
                if (!_originalMaterials.ContainsKey(targetObjectName))
                {
                    var gameObject = Launcher.GetResources().GetGameObject(targetObjectName);
                    var meshRenderer = gameObject.GetComponent<MeshRenderer>();
                    if (meshRenderer == null)
                    {
                        meshRenderer = gameObject.GetComponentInChildren<MeshRenderer>();
                    }

                    _originalMaterials.Add(targetObjectName, meshRenderer.materials);
                }
            }
        }

        private void RestoreMaterials()
        {
            foreach (var originalMaterial in _originalMaterials)
            {
                var targetObjectName = originalMaterial.Key;
                var gameObject = Launcher.GetResources().GetGameObject(targetObjectName);
                var meshRenderer = gameObject.GetComponent<MeshRenderer>();
                if (meshRenderer == null)
                {
                    var componentsInChildren = gameObject.GetComponentsInChildren<MeshRenderer>();
                    foreach (var renderer in componentsInChildren)
                    {
                        renderer.materials = originalMaterial.Value;
                    }
                }
                else
                {
                    meshRenderer.materials = originalMaterial.Value;
                }
            }
        }

        [Serializable]
        private struct MaterialChangeByTimeInfo
        {
            public float time;
            public string targetObjectName;
            public Material[] materials;
        }
    }
}