using System;
using System.Collections.Generic;
using System.Linq;
using CombustionVR.Models.Interactive_Objects.Scripts.Misc;
using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    /// <summary>
    /// Plays once selected audio clip on specified source, set materials from MaterialSet by time and passes to next step.
    /// </summary>
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create PlayAudioAndSetMaterialSetByTime",
        fileName = "Play Audio And Set Material Set By Time Step",
        order = 0)]
    public class PlayAudioAndSetMaterialSetByTime : PlayAudioClip
    {
        [SerializeField] private MaterialSetChangeByTimeInfo[] timeInfoKeys;
        [SerializeField] private bool resetToZeroOnStop = true;
        private Queue<MaterialSetChangeByTimeInfo> _keysQueue;
        private readonly HashSet<MaterialSet> _changedObjects = new HashSet<MaterialSet>();

        public override void Launch(IStepLauncher launcher)
        {
            base.Launch(launcher);
            
            if (!skipAudio)
            {
                _keysQueue = new Queue<MaterialSetChangeByTimeInfo>(timeInfoKeys.OrderBy(info => info.time));
                if (resetToZeroOnStop)
                {
                    BackupMaterials();
                }
            }
        }

        protected override void Update()
        {
            base.Update();

            if (_isLaunched)
            {
                CheckForTimedKeyInQueue();
            }
        }

        private void CheckForTimedKeyInQueue()
        {
            while (_keysQueue.Count > 0 && _keysQueue.Peek().time < AudioSource.time)
            {
                var timeInfoKey = _keysQueue.Dequeue();
                var targetSet = Launcher.GetResources().GetGameObject(timeInfoKey.targetObjectName)
                    .GetComponent<MaterialSet>();
                targetSet.Set(timeInfoKey.setIndex);
            }
        }

        public override void Stop(IStepLauncher launcher)
        {
            if (resetToZeroOnStop)
            {
                RestoreMaterials();
            }

            base.Stop(launcher);
        }

        private void BackupMaterials()
        {
            foreach (var timeInfoKey in timeInfoKeys)
            {
                var targetSet = Launcher.GetResources().GetGameObject(timeInfoKey.targetObjectName)
                    .GetComponent<MaterialSet>();
                if (!_changedObjects.Contains(targetSet))
                {
                    _changedObjects.Add(targetSet);
                }
            }
        }

        private void RestoreMaterials()
        {
            foreach (var materialSet in _changedObjects)
            {
                materialSet.Set(0);
            }
        }

        [Serializable]
        private struct MaterialSetChangeByTimeInfo
        {
            public float time;
            public string targetObjectName;
            public int setIndex;
        }
    }
}