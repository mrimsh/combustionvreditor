using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using CombustionVR.XRController.Scripts;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create SetRigWear", fileName = "Set Rig Wear Step", order = 0)]
    public class SetRigWear : ScenarioStep
    {
        [SerializeField] private bool changeHat;
        [SerializeField] private bool changeHands;
        [SerializeField] private GameObject hat;
        [SerializeField] private HandModel hands;

        public override bool IsLaunched()
        {
            return false;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            var xrController = FindObjectOfType<XRController.Scripts.XRController>();

            if (changeHat)
            {
                xrController.SetHatModel(hat);
            }

            if (changeHands)
            {
                xrController.SetHandModel(hands);
            }

            launcher.StepFinished(this);
        }

        public override void Stop(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}