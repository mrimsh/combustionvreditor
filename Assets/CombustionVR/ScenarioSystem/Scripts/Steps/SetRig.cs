using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using CombustionVR.XRController.Scripts;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create SetRig", fileName = "Set Rig Step", order = 0)]
    public class SetRig : ScenarioStep
    {
        [SerializeField] private bool allowTeleportation;

        public override bool IsLaunched()
        {
            return false;
        }

        public override void Launch(IStepLauncher launcher)
        {
            #if DEBUG_SCENARIO
Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            var raycasters = FindObjectsOfType<XrHandRaycaster>();
            foreach (var raycaster in raycasters)
            {
                raycaster.visualizeAndInteractParabolic = allowTeleportation;
            }

            launcher.StepFinished(this);
        }

        public override void Stop(IStepLauncher launcher)
        {
            #if DEBUG_SCENARIO
Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}