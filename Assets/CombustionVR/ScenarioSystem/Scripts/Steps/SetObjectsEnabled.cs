using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create SetObjectsEnabled", fileName = "Set Objects Enabled Step",
        order = 0)]
    internal class SetObjectsEnabled : ScenarioStep
    {
        [Tooltip("Set Active object if TRUE and Set Disable if FALSE")] [SerializeField]
        private bool enable = true;

        [SerializeField] private string[] objectsToSet;

        public override bool IsLaunched()
        {
            return false;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            var gameObjects = launcher.GetResources().GetGameObjects(objectsToSet);

            foreach (var gameObject in gameObjects)
            {
                gameObject.SetActive(enable);
            }

            launcher.StepFinished(this);
        }

        public override void Stop(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}