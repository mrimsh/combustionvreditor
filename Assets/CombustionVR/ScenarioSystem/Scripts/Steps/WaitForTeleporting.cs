﻿using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using CombustionVR.Teleport.Scripts;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create WaitForTeleporting",
        fileName = "Wait For Teleporting Step",
        order = 0)]
    public class WaitForTeleporting : ScenarioStep
    {
        [SerializeField] private string targetTeleportName;
        [SerializeField] private AudioClip audioToRepeatOnWait;
        [SerializeField] private string audioSourceName;
        [SerializeField] private float audioRepeatTime;

        private bool _isLaunched;
        private IStepLauncher _launcher;
        private float _previousAudioPlayTime;
        private AudioSource _audioSource;

        public override bool IsLaunched()
        {
            return _isLaunched;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            _launcher = launcher;

            _launcher.GetResources().GetGameObject(targetTeleportName).GetComponent<TeleportZone>().TeleportActivated += OnTeleportActivated;

            _audioSource = _launcher.GetResources().GetAudioSource(audioSourceName);
            _audioSource.clip = audioToRepeatOnWait;
            _audioSource.loop = false;

            _previousAudioPlayTime = Time.time;
            launcher.UpdatedStep += Update;

            _isLaunched = true;
        }

        private void OnTeleportActivated(TeleportZone teleportZone)
        {
            _launcher.StepFinished(this);
        }

        private void Update()
        {
            if (_previousAudioPlayTime + audioRepeatTime < Time.time)
            {
                _audioSource.Play();
                _previousAudioPlayTime = Time.time;
            }
        }

        public override void Stop(IStepLauncher launcher)
        {
            if (launcher.UpdatedStep == Update)
            {
                _audioSource.Stop();
                launcher.UpdatedStep = null;
            }
            
            _isLaunched = false;

#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}