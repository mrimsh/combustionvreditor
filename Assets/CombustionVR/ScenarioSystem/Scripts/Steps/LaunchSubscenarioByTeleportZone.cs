using System.Collections.Generic;
using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using CombustionVR.Teleport.Scripts;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    /// <summary>
    /// Plays once selected audio clip on specified source and passes to next step.
    /// </summary>
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create LaunchSubscenarioByTeleportZone",
        fileName = "Launch Subscenario By Teleport Zone Step",
        order = 0)]
    public class LaunchSubscenarioByTeleportZone : ScenarioStep
    {
        [SerializeField] private string subplayerName;
        [SerializeField] private string[] teleportNames;
        [SerializeField] private ScenarioScript[] scenarios;

        [SerializeField] private string rigGoName;
        [SerializeField] private string initialTeleportGoName;

        private List<TeleportZone> _teleportZones;
        private ScenarioPlayer _subplayer;
        private IStepLauncher _launcher;
        private bool _isLaunched;

        public override bool IsLaunched()
        {
            return _isLaunched;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            _launcher = launcher;
            // _launcher.UpdatedStep = Update;
            _subplayer = launcher.GetResources().GetGameObject(subplayerName).GetComponent<ScenarioPlayer>();

            _teleportZones = new List<TeleportZone>();
            foreach (var teleportName in teleportNames)
            {
                var teleportZone = launcher.GetResources().GetGameObject(teleportName).GetComponent<TeleportZone>();
                _teleportZones.Add(teleportZone);
                teleportZone.TeleportActivated += OnTeleportActivated;
            }

            var xrRig = launcher.GetResources().GetGameObject(rigGoName)
                .GetComponent<XRController.Scripts.XRController>();
            var initialTeleportZone = launcher.GetResources().GetGameObject(initialTeleportGoName)
                .GetComponent<TeleportZone>();
            initialTeleportZone.Teleport(xrRig);

            _isLaunched = true;
        }

        // private void Update()
        // {
        // }

        public override void Stop(IStepLauncher launcher)
        {
            foreach (var teleportZone in _teleportZones)
            {
                teleportZone.TeleportActivated -= OnTeleportActivated;
            }

            // if (launcher.UpdatedStep == Update)
            // {
            //     launcher.UpdatedStep = null;
            // }

            _subplayer.StopScenario(true);

            _isLaunched = false;

#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }

        private void OnTeleportActivated(TeleportZone teleportZone)
        {
            _subplayer.StartScenario(scenarios[_teleportZones.IndexOf(teleportZone)]);
        }
    }
}