using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    /// <summary>
    /// Plays once selected animation on specified gameobject and passes to next step.
    /// </summary>
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create PlayAnim", fileName = "Play Anim Step",
        order = 0)]
    public class PlayAnim : ScenarioStep
    {
        [SerializeField] private string targetGameObjectName;
        [SerializeField] private AnimationClip clipToPlay;

        /// <summary>
        /// Cached Animation component of target
        /// </summary>
        private Animation _animation;
        private IStepLauncher _launcher;
        private bool _isLaunched;

        public override bool IsLaunched()
        {
            return _isLaunched;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            _launcher = launcher;
            _launcher.UpdatedStep = Update;

            _animation = _launcher.GetResources().GetGameObject(targetGameObjectName).GetComponent<Animation>();
            _animation.clip = clipToPlay;
            _animation.Play();
            
            _isLaunched = true;
        }

        private void Update()
        {
            if (!_animation.isPlaying && _isLaunched)
            {
                _launcher.StepFinished(this);
            }
        }

        public override void Stop(IStepLauncher launcher)
        {
            if (launcher.UpdatedStep == Update)
            {
                launcher.UpdatedStep = null;
            }
            
            _animation.Stop();
            _isLaunched = false;

#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}