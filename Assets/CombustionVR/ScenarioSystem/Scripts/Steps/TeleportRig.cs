using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using CombustionVR.Teleport.Scripts;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create TeleportRig", fileName = "Teleport Rig Step",
        order = 0)]
    internal class TeleportRig : ScenarioStep
    {
        [SerializeField] private string rigGoName;
        [SerializeField] private string targetTeleportGoName;

        public override bool IsLaunched()
        {
            return false;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            var xrRig = launcher.GetResources().GetGameObject(rigGoName)
                .GetComponent<XRController.Scripts.XRController>();
            var teleportZone = launcher.GetResources().GetGameObject(targetTeleportGoName).GetComponent<TeleportZone>();

            teleportZone.Teleport(xrRig);

            launcher.StepFinished(this);
        }

        public override void Stop(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}