using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using CombustionVR.XRController.Scripts;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create SetHoverableGrabable",
        fileName = "Set Hoverable Grabable Step",
        order = 0)]
    internal class SetHoverableGrabable : ScenarioStep
    {
        [SerializeField] private bool hoverable;
        [SerializeField] private bool grabable;


        [SerializeField] private string[] objectsToSet;

        public override bool IsLaunched()
        {
            return false;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif
            var gameObjects = launcher.GetResources().GetGameObjects(objectsToSet);

            foreach (var gameObject in gameObjects)
            {
                var hoverableObject = gameObject.GetComponent<IHoverable>();
                if (hoverableObject is IGrabable grabableObject)
                {
                    grabableObject.IsGrabable = grabable;
                }
                if (hoverableObject != null)
                {
                    hoverableObject.IsHoverable = hoverable;
                }
            }

            launcher.StepFinished(this);
        }

        public override void Stop(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}