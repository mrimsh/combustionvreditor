using CombustionVR.ScenarioSystem.Scripts.Interfaces;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts.Steps
{
    /// <summary>
    /// Plays once selected audio clip on specified source and passes to next step.
    /// </summary>
    [CreateAssetMenu(menuName = "ScenarioSystem/Steps/Create PlayAudioClip", fileName = "Play Audio Clip Step",
        order = 0)]
    public class PlayAudioClip : ScenarioStep
    {
        public static bool skipAudio;

        [SerializeField] private string audioSourceName;
        [SerializeField] private AudioClip clipToPlay;

        protected AudioSource AudioSource;
        protected IStepLauncher Launcher;
        protected bool _isLaunched;

        public override bool IsLaunched()
        {
            return _isLaunched;
        }

        public override void Launch(IStepLauncher launcher)
        {
#if DEBUG_SCENARIO
            Debug.Log("<color=green>Launching</color> " + name + " step");
#endif

            if (skipAudio)
            {
                Launcher = launcher;
                Launcher.UpdatedStep = Update;
                AudioSource = Launcher.GetResources().GetAudioSource(audioSourceName);
                AudioSource.Stop();
                _isLaunched = true;
            }
            else
            {
                Launcher = launcher;
                Launcher.UpdatedStep = Update;

                AudioSource = Launcher.GetResources().GetAudioSource(audioSourceName);
                AudioSource.clip = clipToPlay;
                AudioSource.loop = false;
                AudioSource.Play();
                _isLaunched = true;
            }
        }

        protected virtual void Update()
        {
            if (!AudioSource.isPlaying && _isLaunched)
            {
                Launcher.StepFinished(this);
            }
        }

        public override void Stop(IStepLauncher launcher)
        {
            if (launcher.UpdatedStep == Update)
            {
                AudioSource.Stop();
                launcher.UpdatedStep = null;
            }

            _isLaunched = false;

#if DEBUG_SCENARIO
            Debug.Log(name + " <color=red>step ended</color>");
#endif
        }
    }
}