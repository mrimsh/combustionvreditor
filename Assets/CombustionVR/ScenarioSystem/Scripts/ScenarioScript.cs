using System.Collections.Generic;
using CombustionVR.ScenarioSystem.Scripts.Steps;
using Malee.List;
using UnityEngine;

namespace CombustionVR.ScenarioSystem.Scripts
{
    [CreateAssetMenu(menuName = "ScenarioSystem/Create ScenarioScript", fileName = "ScenarioScript", order = 0)]
    public class ScenarioScript : ScriptableObject
    {
        [Reorderable] [SerializeField] internal StepsList steps;
        [Reorderable] [SerializeField] internal StepsList finalizingSteps;
    }

    [System.Serializable]
    public class StepsList : ReorderableArray<ScenarioStep>
    {
    }
}